import flood_fill
import pytest

def test_parse_map():
    expected =[[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
               [2, 0, 0, 0, 1, 0, 0, 1, 0, 0],
               [1, 1, 0, 1, 0, 0, 0, 1, 0, 1],
               [1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
               [1, 0, 1, 1, 0, 0, 0, 1, 0, 1],
               [1, 0, 1, 1, 0, 1, 0, 1, 0, 1],
               [1, 0, 1, 0, 0, 1, 0, 1, 0, 1],
               [1, 0, 0, 0, 0, 1, 0, 1, 0, 1],
               [1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
               [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]

    actual = flood_fill.parse_map("maze2.txt")
    assert actual == expected
    #now let's try a file that doesn't exist
    # expected = []
    # actual = flood_fill.parse_map("lkj nc qgfwcaelkjh.txt")
    # assert expected == actual
